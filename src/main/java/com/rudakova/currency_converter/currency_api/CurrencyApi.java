package com.rudakova.currency_converter.currency_api;

import com.rudakova.currency_converter.model.Currency;
import com.rudakova.currency_converter.model.HistoryRecord;
import com.rudakova.currency_converter.model.Rate;
import com.rudakova.currency_converter.repository.HistoryRecordRepository;
import com.rudakova.currency_converter.service.AuthService;
import com.rudakova.currency_converter.service.XmlParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

@Service
public class CurrencyApi {
    private static final Logger LOG = LoggerFactory.getLogger(CurrencyApi.class);
    public static final HttpClient httpClient;

    @Autowired
    private AuthService authService;

    @Autowired
    private XmlParser xmlParser;

    @Autowired
    private HistoryRecordRepository historyRecordRepository;

    static {
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .build();
    }

    public void getCurrency() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Instant instant = LocalDate.now()
                .atStartOfDay()
                .toInstant(ZoneOffset.UTC);
        String date = simpleDateFormat.format(Date.from(instant));
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + date))
                .GET()
                .build();
        try {
            File file = File.createTempFile("response", ".xml");
            httpClient.send(request, HttpResponse.BodyHandlers.ofFile(file.toPath()));
            xmlParser.parse(file.toPath());
            file.delete();
        } catch (IOException | InterruptedException e) {
            LOG.error("Error in send request to cbr: {}", e.getMessage());
        }
    }

    public double convert(Currency original, Currency target, double originalValue) {
        Rate originalRate = original.getRates().get(0);
        Rate targetRate = target.getRates().get(0);

        LocalDate originalLastUpdate = LocalDate.ofInstant(originalRate.getDate().toInstant(), ZoneId.systemDefault());
        LocalDate targetLastUpdate = LocalDate.ofInstant(originalRate.getDate().toInstant(), ZoneId.systemDefault());

        // Если данных за сегодня нет, то обновляем их
        if (originalLastUpdate.isBefore(LocalDate.now()) || targetLastUpdate.isBefore(LocalDate.now()))  {
            getCurrency();
        }

        double targetValue = originalValue *
                (originalRate.getValue() * target.getNominal() /
                        targetRate.getValue() * original.getNominal());

        HistoryRecord historyRecord = new HistoryRecord();
        historyRecord.setOriginal(originalRate);
        historyRecord.setTarget(targetRate);
        historyRecord.setDate(LocalDateTime.now());
        historyRecord.setOriginalValue(originalValue);
        historyRecord.setTargetValue(targetValue);
        historyRecord.setAccount(authService.getLoggedInUser());
        historyRecordRepository.save(historyRecord);

        return targetValue;
    }
}
