package com.rudakova.currency_converter.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class HistoryRecord extends AbstractPersistable<Long> {

    @ManyToOne
    private Rate original;

    @ManyToOne
    private Rate target;

    @Column(nullable = false)
    private double originalValue;

    @Column(nullable = false)
    private double targetValue;

    @Column(nullable = false)
    private LocalDateTime date;

    @ManyToOne
    private Account account;

    public Rate getOriginal() {
        return original;
    }

    public void setOriginal(Rate original) {
        this.original = original;
    }

    public Rate getTarget() {
        return target;
    }

    public void setTarget(Rate target) {
        this.target = target;
    }

    public double getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(double originalValue) {
        this.originalValue = originalValue;
    }

    public double getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(double targetValue) {
        this.targetValue = targetValue;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
