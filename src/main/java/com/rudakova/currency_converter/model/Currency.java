package com.rudakova.currency_converter.model;

import org.hibernate.annotations.OrderBy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Currency {

    @Id
    private String id;

    @Column(nullable = false, unique = true)
    private String numCode;

    @Column(nullable = false, unique = true)
    private String charCode;

    @Column(nullable = false)
    private int nominal;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "currency")
    @OrderBy(clause = "date DESC")
    private List<Rate> rates;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumCode() {
        return numCode;
    }

    public void setNumCode(String numCode) {
        this.numCode = numCode;
    }

    public String getCharCode() {
        return charCode;
    }

    public void setCharCode(String charCode) {
        this.charCode = charCode;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(List<Rate> rates) {
        this.rates = rates;
    }
}
