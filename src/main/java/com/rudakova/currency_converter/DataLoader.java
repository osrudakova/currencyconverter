package com.rudakova.currency_converter;

import com.rudakova.currency_converter.currency_api.CurrencyApi;
import com.rudakova.currency_converter.model.Currency;
import com.rudakova.currency_converter.model.Rate;
import com.rudakova.currency_converter.repository.CurrencyRepository;
import com.rudakova.currency_converter.repository.RateRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;

@Component
public class DataLoader implements InitializingBean {

    @Autowired
    private CurrencyApi currencyApi;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private RateRepository rateRepository;

    @Override
    public void afterPropertiesSet() {

        if (currencyRepository.count() == 0) {
            Currency currency = new Currency();
            currency.setId("RUB");
            currency.setNominal(1);
            currency.setCharCode("RUB");
            currency.setNumCode("643");
            currency.setName("Российский рубль");
            currencyRepository.save(currency);

            Rate rate = new Rate();
            rate.setCurrency(currency);
            rate.setDate(Date.from(Instant.now()));
            rate.setValue(1.0);
            rateRepository.save(rate);
        }

        currencyApi.getCurrency();
    }
}
