package com.rudakova.currency_converter.service;

import com.rudakova.currency_converter.model.Account;
import com.rudakova.currency_converter.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private AccountRepository accountRepository;

    /**
     * Получение авторизованного пользователя.
     */
    public Account getLoggedInUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getName().equals("anonymousUser")) {
            return null;
        }
        return accountRepository.getAccountByLogin(authentication.getName()).get();
    }
}
