package com.rudakova.currency_converter.service;

import com.rudakova.currency_converter.model.Currency;
import com.rudakova.currency_converter.model.Rate;
import com.rudakova.currency_converter.repository.CurrencyRepository;
import com.rudakova.currency_converter.repository.RateRepository;
import com.rudakova.currency_converter.web.ConverterPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Date;

@Service
public class XmlParser {
    private static final Logger LOG = LoggerFactory.getLogger(XmlParser.class);

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private RateRepository rateRepository;

    public void parse(Path currencyFile) {
        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(currencyFile.toFile());

            Node root = document.getDocumentElement();
            NodeList currencies = root.getChildNodes();

            for (int i = 0; i < currencies.getLength(); i++) {
                Node currencyNode = currencies.item(i);
                NodeList currencyProps = currencyNode.getChildNodes();
                String id = currencyNode.getAttributes().getNamedItem("ID").getNodeValue();

                Currency currency = currencyRepository.findById(id).orElse(new Currency());
                currency.setId(id);
                currency.setNumCode(getProperty(currencyProps, 0));
                currency.setCharCode(getProperty(currencyProps, 1));
                currency.setNominal(Integer.parseInt(getProperty(currencyProps, 2)));
                currency.setName(getProperty(currencyProps, 3));
                currencyRepository.save(currency);

                String value = getProperty(currencyProps, 4).replace(",", ".");

                Rate rate = new Rate();
                rate.setCurrency(currency);
                rate.setDate(Date.from(Instant.now()));
                rate.setValue(Double.parseDouble(value));
                rateRepository.save(rate);

            }

        } catch (ParserConfigurationException | IOException | SAXException ex) {
            LOG.error("Error in parse xml: {}", ex.getMessage());
        }
    }

    private String getProperty(NodeList nodeList, int index) {
        return nodeList.item(index).getChildNodes().item(0).getNodeValue();
    }
}
