package com.rudakova.currency_converter.repository;

import com.rudakova.currency_converter.model.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {

    @Query("FROM Account WHERE login = :login")
    Optional<Account> getAccountByLogin(@Param("login") String login);
}
