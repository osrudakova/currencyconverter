package com.rudakova.currency_converter.repository;

import com.rudakova.currency_converter.model.Account;
import com.rudakova.currency_converter.model.Currency;
import com.rudakova.currency_converter.model.HistoryRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface HistoryRecordRepository extends CrudRepository<HistoryRecord, Long> {

    @Query("FROM HistoryRecord WHERE account = :account ORDER BY date DESC")
    List<HistoryRecord> findAll(@Param("account") Account account);

    @Query("FROM HistoryRecord WHERE account = :account " +
            "AND (:original IS NULL OR original.currency = :original) " +
            "AND (:target IS NULL OR target.currency = :target) " +
            "AND (date BETWEEN :startDate AND :endDate) " +
            "ORDER BY date DESC")
    List<HistoryRecord> search(@Param("account") Account account, @Param("original") Currency original,
                                @Param("target") Currency target, @Param("startDate") LocalDateTime startDate,
                               @Param("endDate") LocalDateTime endDate);
}
