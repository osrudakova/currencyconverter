package com.rudakova.currency_converter.repository;

import com.rudakova.currency_converter.model.Currency;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CurrencyRepository extends CrudRepository<Currency, String> {

    @Override
    @Query("FROM Currency ORDER BY name")
    List<Currency> findAll();

    @Query("FROM Currency WHERE id = :id")
    Optional<Currency> findById(String id);
}
