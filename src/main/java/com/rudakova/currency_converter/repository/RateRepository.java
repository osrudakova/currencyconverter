package com.rudakova.currency_converter.repository;

import com.rudakova.currency_converter.model.Rate;
import org.springframework.data.repository.CrudRepository;

public interface RateRepository extends CrudRepository<Rate, Long> {
}
