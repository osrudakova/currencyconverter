package com.rudakova.currency_converter.web;

import com.rudakova.currency_converter.model.Account;
import com.rudakova.currency_converter.repository.AccountRepository;
import com.rudakova.currency_converter.web.wrapper.RegistrationDataWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class RegistrationPage {
    private static final Logger LOG = LoggerFactory.getLogger(RegistrationPage.class);

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping
    public String registrationPage(Model model) {
        model.addAttribute("registrationDataWrapper", new RegistrationDataWrapper());
        return "registration";
    }

    @PostMapping
    public String registration(Model model, @ModelAttribute("registrationDataWrapper") RegistrationDataWrapper wrapper) {
        Account account = new Account();
        account.setLogin(wrapper.getLogin());
        account.setPassword(wrapper.getPassword());
        accountRepository.save(account);
        return "login";
    }
}
