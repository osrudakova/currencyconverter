package com.rudakova.currency_converter.web;

import com.rudakova.currency_converter.currency_api.CurrencyApi;
import com.rudakova.currency_converter.model.Account;
import com.rudakova.currency_converter.model.Currency;
import com.rudakova.currency_converter.repository.CurrencyRepository;
import com.rudakova.currency_converter.repository.HistoryRecordRepository;
import com.rudakova.currency_converter.service.AuthService;
import com.rudakova.currency_converter.web.wrapper.ConverterWrapper;
import com.rudakova.currency_converter.web.wrapper.SearchWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Controller
@RequestMapping("/")
public class ConverterPage {
    private static final Logger LOG = LoggerFactory.getLogger(ConverterPage.class);

    @Autowired
    private AuthService authService;

    @Autowired
    private CurrencyApi currencyApi;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private HistoryRecordRepository historyRecordRepository;

    @GetMapping
    public String converterPage(Model model) {
        createModel(model, new ConverterWrapper(), new SearchWrapper());
        return "converterPage";
    }

    @PostMapping("/convert")
    public String convert(Model model, @ModelAttribute("converterWrapper") ConverterWrapper converterWrapper) {
        Currency original = currencyRepository.findById(converterWrapper.getOriginal()).get();
        Currency target = currencyRepository.findById(converterWrapper.getTarget()).get();
        double originalValue = converterWrapper.getOriginalValue();
        double targetValue = currencyApi.convert(original, target, originalValue);

        converterWrapper.setTargetValue(targetValue);
        createModel(model, converterWrapper, new SearchWrapper());
        return "converterPage";
    }

    @PostMapping("/search")
    public String search(Model model, @ModelAttribute("searchWrapper") SearchWrapper searchWrapper) {
        createModel(model, new ConverterWrapper(), searchWrapper);
        return "converterPage";
    }

    private void createModel(Model model, ConverterWrapper converterWrapper, SearchWrapper searchWrapper) {
        Account account = authService.getLoggedInUser();
        model.addAttribute("account", account);
        model.addAttribute("currencies", currencyRepository.findAll());
        model.addAttribute("converterWrapper", converterWrapper);
        model.addAttribute("searchWrapper", searchWrapper);

        if ((hasValue(searchWrapper.getStartDate()) && hasValue(searchWrapper.getEndDate())) ||
                hasValue(searchWrapper.getOriginal()) || hasValue(searchWrapper.getTarget())) {
            Currency original = searchWrapper.getOriginal().equals("") ? null :
                    currencyRepository.findById(searchWrapper.getOriginal()).get();
            Currency target = searchWrapper.getTarget().equals("") ? null :
                    currencyRepository.findById(searchWrapper.getTarget()).get();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime startDate = hasValue(searchWrapper.getStartDate()) ?
                    LocalDateTime.parse(searchWrapper.getStartDate() + " 00:00", formatter) : null;
            LocalDateTime endDate = hasValue(searchWrapper.getEndDate()) ?
                    LocalDateTime.parse(searchWrapper.getEndDate() + " 23:59", formatter) : null;
            model.addAttribute("history", historyRecordRepository.search(account, original,
                    target, startDate, endDate));
        } else {
            model.addAttribute("history", historyRecordRepository.findAll(account));
        }
    }

    private boolean hasValue(String str) {
        return str != null && !str.equals("");
    }
}
